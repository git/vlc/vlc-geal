/*****************************************************************************
 * dxva.h: DXVA 2 video decoder
 *****************************************************************************
 * Copyright (C) 2009 the VideoLAN team
 * $Id$
 *
 * Authors: Geoffroy Couprie <geal@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef _DXVA_H
#define _DXVA_H

#if __GNUC__ >=3
#pragma GCC system_header
#endif

#include <objbase.h>
#include <d3d9.h>

/****************STRUCTURES******************/
typedef struct _DXVA2_ExtendedFormat {
  union {
    struct {
      UINT SampleFormat  :8;
      UINT VideoChromaSubsampling  :4;
      UINT NominalRange  :3;
      UINT VideoTransferMatrix  :3;
      UINT VideoLighting  :4;
      UINT VideoPrimaries  :5;
      UINT VideoTransferFunction  :5;
    } ;
    UINT value;
  } ;
} DXVA2_ExtendedFormat;

typedef struct _DXVA2_Frequency {
  UINT Numerator;
  UINT Denominator;
} DXVA2_Frequency;

typedef struct _DXVA2_VideoDesc {
  UINT SampleWidth;
  UINT SampleHeight;
  DXVA2_ExtendedFormat SampleFormat;
  D3DFORMAT Format;
  DXVA2_Frequency InputSampleFreq;
  DXVA2_Frequency OutputFrameFreq;
  UINT UABProtectionLevel;
  UINT Reserved;
} DXVA2_VideoDesc;

typedef struct _DXVA2_ConfigPictureDecode {
  GUID guidConfigBitstreamEncryption;
  GUID guidConfigMBcontrolEncryption;
  GUID guidConfigResidDiffEncryption;
  UINT ConfigBitstreamRaw;
  UINT ConfigMBcontrolRasterOrder;
  UINT ConfigResidDiffHost;
  UINT ConfigSpatialResid8;
  UINT ConfigResid8Subtraction;
  UINT ConfigSpatialHost8or9Clipping;
  UINT ConfigSpatialResidInterleaved;
  UINT ConfigIntraResidUnsigned;
  UINT ConfigResidDiffAccelerator;
  UINT ConfigHostInverseScan;
  UINT ConfigSpecificIDCT;
  UINT Config4GroupedCoefs;
  UINT ConfigMinRenderTargetBuffCount;
  USHORT ConfigDecoderSpecific;
} DXVA2_ConfigPictureDecode;

typedef struct _DXVA2_DecodeBufferDesc {
  DWORD CompressedBufferType;
  UINT BufferIndex;
  UINT DataOffset;
  UINT DataSize;
  UINT FirstMBaddress;
  UINT NumMBsInBuffer;
  UINT Width;
  UINT Height;
  UINT Stride;
  UINT ReservedBits;
  PVOID pvPVPState;
} DXVA2_DecodeBufferDesc;

typedef struct _DXVA2_DecodeExtensionData {
  UINT Function;
  PVOID pPrivateInputData;
  UINT PrivateInputDataSize;
  PVOID pPrivateOutputData;
  UINT PrivateOutputDataSize;
} DXVA2_DecodeExtensionData;

typedef struct _DXVA2_DecodeExecuteParams {
  UINT NumCompBuffers;
  DXVA2_DecodeBufferDesc *pCompressedBuffers;
  DXVA2_DecodeExtensionData *pExtensionData;
} DXVA2_DecodeExecuteParams;

enum {
    DXVA2_VideoDecoderRenderTarget	= 0,
	DXVA2_VideoProcessorRenderTarget	= 1,
	DXVA2_VideoSoftwareRenderTarget	= 2
};

enum {
    DXVA2_PictureParametersBufferType	= 0,
	DXVA2_MacroBlockControlBufferType	= 1,
	DXVA2_ResidualDifferenceBufferType	= 2,
	DXVA2_DeblockingControlBufferType	= 3,
	DXVA2_InverseQuantizationMatrixBufferType	= 4,
	DXVA2_SliceControlBufferType	= 5,
	DXVA2_BitStreamDateBufferType	= 6,
	DXVA2_MotionVectorBuffer	= 7,
	DXVA2_FilmGrainBuffer	= 8
};


/*************INTERFACES************/
#ifdef __cplusplus
extern "C" {
#endif

//extern const GUID IID_IDirectXVideoDecoderService;


typedef _COM_interface IDirectXVideoDecoderService IDirectXVideoDecoderService;
typedef _COM_interface IDirectXVideoDecoder IDirectXVideoDecoder;

#undef INTERFACE
#define INTERFACE IDirectXVideoDecoder
DECLARE_INTERFACE_(IDirectXVideoDecoder,IUnknown)
{
	STDMETHOD(QueryInterface)(THIS_ REFIID,PVOID*) PURE;
	STDMETHOD_(ULONG,AddRef)(THIS) PURE;
	STDMETHOD_(ULONG,Release)(THIS) PURE;
	STDMETHOD(GetVideoDecoderService)(THIS_ IDirectXVideoDecoderService**) PURE;
	STDMETHOD(GetCreationParameters)(THIS_ GUID*,DXVA2_VideoDesc*,DXVA2_ConfigPictureDecode*,IDirect3DSurface9***,UINT*) PURE;
	STDMETHOD(GetBuffer)(THIS_ UINT,void**,UINT*) PURE;
	STDMETHOD(ReleaseBuffer)(THIS_ UINT) PURE;
	STDMETHOD(BeginFrame)(THIS_ IDirect3DSurface9 *,void*) PURE;
	STDMETHOD(EndFrame)(THIS_ HANDLE *) PURE;
	STDMETHOD(Execute)(THIS_ const DXVA2_DecodeExecuteParams*) PURE;


};
typedef struct IDirectXVideoDecoder *LPDIRECTXVIDEODECODER, *PDIRECTXVIDEODECODER;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirectXVideoDecoder_QueryInterface(p,a,b)	(p)->lpVtbl->QueryInterface(p,a,b)
#define IDirectXVideoDecoder_AddRef(p)	(p)->lpVtbl->AddRef(p)
#define IDirectXVideoDecoder_Release(p)	(p)->lpVtbl->Release(p)
#define IDirectXVideoDecoder_BeginFrame(p,a,b)	(p)->lpVtbl->BeginFrame(p,a,b)
#define IDirectXVideoDecoder_EndFrame(p,a)	(p)->lpVtbl->EndFrame(p,a)
#define IDirectXVideoDecoder_Execute(p,a)	(p)->lpVtbl->Execute(p,a)
#define IDirectXVideoDecoder_GetBuffer(p,a,b,c)	(p)->lpVtbl->GetBuffer(p,a,b,c)
#define IDirectXVideoDecoder_GetCreationParameters(p,a,b,c,d,e)	(p)->lpVtbl->GetCreationParameters(p,a,b,c,d,e)
#define IDirectXVideoDecoder_GetVideoDecoderService(p,a)	(p)->lpVtbl->GetVideoDecoderService(p,a)
#define IDirectXVideoDecoder_ReleaseBuffer(p,a)	(p)->lpVtbl->ReleaseBuffer(p,a)
#else
#define IDirectXVideoDecoder_QueryInterface(p,a,b)	(p)->QueryInterface(a,b)
#define IDirectXVideoDecoder_AddRef(p)	(p)->AddRef()
#define IDirectXVideoDecoder_Release(p)	(p)->Release()
#define IDirectXVideoDecoder_BeginFrame(p,a,b)	(p)->BeginFrame(a,b)
#define IDirectXVideoDecoder_EndFrame(p,a)	(p)->EndFrame(a)
#define IDirectXVideoDecoder_Execute(p,a)	(p)->Execute(a)
#define IDirectXVideoDecoder_GetBuffer(p,a,b,c)	(p)->GetBuffer(a,b,c)
#define IDirectXVideoDecoder_GetCreationParameters(p,a,b,c,d,e)	(p)->GetCreationParameters(a,b,c,d,e)
#define IDirectXVideoDecoder_GetVideoDecoderService(p,a)	(p)->GetVideoDecoderService(a)
#define IDirectXVideoDecoder_ReleaseBuffer(p,a)	(p)->ReleaseBuffer(a)
#endif

#undef INTERFACE
#define INTERFACE IDirectXVideoAccelerationService
DECLARE_INTERFACE_(IDirectXVideoAccelerationService,IUnknown)
{
    STDMETHOD(QueryInterface)(THIS_ REFIID,PVOID*) PURE;
	STDMETHOD_(ULONG,AddRef)(THIS) PURE;
	STDMETHOD_(ULONG,Release)(THIS) PURE;
    STDMETHOD(CreateSurface)(THIS_ UINT,UINT,UINT,D3DFORMAT,D3DPOOL,DWORD,DWORD,IDirect3DSurface9**,HANDLE*) PURE;

};
typedef struct IDirectXVideoAccelerationService *LPDIRECTXVIDEOACCELERATIONSERVICE, *PDIRECTXVIDEOACCELERATIONSERVICE;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirectXVideoAccelerationService_QueryInterface(p,a,b)	(p)->lpVtbl->QueryInterface(p,a,b)
#define IDirectXVideoAccelerationService_AddRef(p)	(p)->lpVtbl->AddRef(p)
#define IDirectXVideoAccelerationService_Release(p)	(p)->lpVtbl->Release(p)
#define IDirectXVideoAccelerationService_CreateSurface(p,a,b,c,d,e,f,g,h,i)	(p)->lpVtbl->CreateSurface(p,a,b,c,d,e,f,g,h,i)
#else
#define IDirectXVideoAccelerationService_QueryInterface(p,a,b)	(p)->QueryInterface(a,b)
#define IDirectXVideoAccelerationService_AddRef(p)	(p)->AddRef()
#define IDirectXVideoAccelerationService_Release(p)	(p)->Release()
#define IDirectXVideoAccelerationService_CreateSurface(p,a,b,c,d,e,f,g,h,i)	(p)->CreateSurface(a,b,c,d,e,f,g,h,i)
#endif

#undef INTERFACE
#define INTERFACE IDirectXVideoDecoderService
DECLARE_INTERFACE_(IDirectXVideoDecoderService,IDirectXVideoAccelerationService)
{
    STDMETHOD(QueryInterface)(THIS_ REFIID,PVOID*) PURE;
	STDMETHOD_(ULONG,AddRef)(THIS) PURE;
	STDMETHOD_(ULONG,Release)(THIS) PURE;
    STDMETHOD(CreateSurface)(THIS_ UINT,UINT,UINT,D3DFORMAT,D3DPOOL,DWORD,DWORD,IDirect3DSurface9**,HANDLE*) PURE;
    STDMETHOD(GetDecoderDeviceGuids)(THIS_ UINT*,GUID **) PURE;
    STDMETHOD(GetDecoderRenderTargets)(THIS_ REFGUID,UINT*,D3DFORMAT**) PURE;
    STDMETHOD(GetDecoderConfigurations)(THIS_ REFGUID,const DXVA2_VideoDesc*,IUnknown*,UINT*,DXVA2_ConfigPictureDecode**) PURE;
    STDMETHOD(CreateVideoDecoder)(THIS_ REFGUID,const DXVA2_VideoDesc*,DXVA2_ConfigPictureDecode*,IDirect3DSurface9**,UINT,IDirectXVideoDecoder**) PURE;
};
typedef struct IDirectXVideoDecoderService *LPDIRECTXVIDEODECODERSERVICE, *PDIRECTXVIDEODECODERSERVICE;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirectXVideoDecoderService_QueryInterface(p,a,b)	(p)->lpVtbl->QueryInterface(p,a,b)
#define IDirectXVideoDecoderService_AddRef(p)	(p)->lpVtbl->AddRef(p)
#define IDirectXVideoDecoderService_Release(p)	(p)->lpVtbl->Release(p)
#define IDirectXVideoDecoderService_CreateSurface(p,a,b,c,d,e,f,g,h,i)	(p)->lpVtbl->CreateSurface(p,a,b,c,d,e,f,g,h,i)
#define IDirectXVideoDecoderService_CreateVideoDecoder(p,a,b,c,d,e,f)	(p)->lpVtbl->CreateVideoDecoder(p,a,b,c,d,e,f)
#define IDirectXVideoDecoderService_GetDecoderConfigurations(p,a,b,c,d,e)	(p)->lpVtbl->GetDecoderConfigurations(p,a,b,c,d,e)
#define IDirectXVideoDecoderService_GetDecoderDeviceGuids(p,a,b)	(p)->lpVtbl->GetDecoderDeviceGuids(p,a,b)
#define IDirectXVideoDecoderService_GetDecoderRenderTargets(p,a,b,c)	(p)->lpVtbl->GetDecoderRenderTargets(p,a,b,c)
#else
#define IDirectXVideoDecoderService_QueryInterface(p,a,b)	(p)->QueryInterface(a,b)
#define IDirectXVideoDecoderService_AddRef(p)	(p)->AddRef()
#define IDirectXVideoDecoderService_Release(p)	(p)->Release()
#define IDirectXVideoDecoderService_CreateSurface(p,a,b,c,d,e,f,g,h,i)	(p)->CreateSurface(a,b,c,d,e,f,g,h,i)
#define IDirectXVideoDecoderService_CreateVideoDecoder(p,a,b,c,d,e,f)	(p)->CreateVideoDecoder(a,b,c,d,e,f)
#define IDirectXVideoDecoderService_GetDecoderConfigurations(p,a,b,c,d,e)	(p)->GetDecoderConfigurations(a,b,c,d,e)
#define IDirectXVideoDecoderService_GetDecoderDeviceGuids(p,a,b)	(p)->GetDecoderDeviceGuids(a,b)
#define IDirectXVideoDecoderService_GetDecoderRenderTargets(p,a,b,c)	(p)->GetDecoderRenderTargets(a,b,c)
#endif

#undef INTERFACE
#define INTERFACE IDirect3DDeviceManager9
DECLARE_INTERFACE_(IDirect3DDeviceManager9,IUnknown)
{
    STDMETHOD(QueryInterface)(THIS_ REFIID,PVOID*) PURE;
	STDMETHOD_(ULONG,AddRef)(THIS) PURE;
	STDMETHOD_(ULONG,Release)(THIS) PURE;
    STDMETHOD(ResetDevice)(THIS_ IDirect3DDevice9*,UINT) PURE;
    STDMETHOD(OpenDeviceHandle)(THIS_ HANDLE*) PURE;
    STDMETHOD(CloseDeviceHandle)( THIS_ HANDLE) PURE;
    STDMETHOD(TestDevice)( THIS_ HANDLE) PURE;
    STDMETHOD(LockDevice)( THIS_ HANDLE,IDirect3DDevice9**,BOOL) PURE;
    STDMETHOD(UnlockDevice)( THIS_ HANDLE,BOOL) PURE;
    STDMETHOD(GetVideoService)( THIS_ HANDLE,REFIID,void**) PURE;
};
typedef struct IDirect3DDeviceManager9 *LPDIRECT3DDEVICEMANAGER9, *PDIRECT3DDEVICEMANAGER9;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DDeviceManager9_QueryInterface(p,a,b)	(p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DDeviceManager9_AddRef(p)	(p)->lpVtbl->AddRef(p)
#define IDirect3DDeviceManager9_Release(p)	(p)->lpVtbl->Release(p)
#define IDirect3DDeviceManager9_ResetDevice(p,a,b) (p)->lpVtbl->ResetDevice(p,a,b)
#define IDirect3DDeviceManager9_OpenDeviceHandle(p,a) (p)->lpVtbl->OpenDeviceHandle(p,a)
#define IDirect3DDeviceManager9_CloseDeviceHandle(p,a) (p)->lpVtbl->CloseDeviceHandle(p,a)
#define IDirect3DDeviceManager9_TestDevice(p,a) (p)->lpVtbl->TestDevice(p,a)
#define IDirect3DDeviceManager9_LockDevice(p,a,b,c) (p)->lpVtbl->LockDevice(p,a,b,c)
#define IDirect3DDeviceManager9_UnlockDevice(p,a,b) (p)->lpVtbl->UnlockDevice(p,a,b)
#define IDirect3DDeviceManager9_GetVideoService(p,a,b,c) (p)->lpVtbl->GetVideoService(p,a,b,c)
#else
#define IDirect3DDeviceManager9_QueryInterface(p,a,b)	(p)->QueryInterface(a,b)
#define IDirect3DDeviceManager9_AddRef(p)	(p)->AddRef()
#define IDirect3DDeviceManager9_Release(p)	(p)->Release()
#define IDirect3DDeviceManager9_ResetDevice(p,a,b) (p)->ResetDevice(a,b)
#define IDirect3DDeviceManager9_OpenDeviceHandle(p,a) (p)->OpenDeviceHandle(a)
#define IDirect3DDeviceManager9_CloseDeviceHandle(p,a) (p)->CloseDeviceHandle(a)
#define IDirect3DDeviceManager9_TestDevice(p,a) (p)->TestDevice(a)
#define IDirect3DDeviceManager9_LockDevice(p,a,b,c) (p)->LockDevice(a,b,c)
#define IDirect3DDeviceManager9_UnlockDevice(p,a,b) (p)->UnlockDevice(a,b)
#define IDirect3DDeviceManager9_GetVideoService(p,a,b,c) (p)->GetVideoService(a,b,c)
#endif

#ifdef __cplusplus
};
#endif

#endif //_DXVA_H